import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import dayjs from 'dayjs';

import { fetechPassengers, editPassenger } from '../../features/passengerSlice';
import { Passenger } from '../../types/types';
import { RootState, AppDispatch } from '../../store';
import PassengerCard from '../../components/PassengerCard';


const Booking = () => {
    const passengers = useSelector((state: RootState) => state.passengers);
    console.log('passengers', passengers);

    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useDispatch<AppDispatch>();

    const handleEditPassenger = (passenger: Passenger) => {
        try {
            dispatch(editPassenger(passenger));
            dispatch(fetechPassengers());

            enqueueSnackbar('The passenger has been updated successfully!', { variant: 'success' });

            return true;
        } catch (error) {
            enqueueSnackbar('Something went wrong.', { variant: 'success' });

            return false;
        }

    };

    useEffect(() => {
        dispatch(fetechPassengers());
    }, []);

    return (
        <>
            {
                passengers.map((passanger) => <PassengerCard
                    id={passanger.id}
                    title={passanger.title}
                    gender={passanger.gender}
                    firstName={passanger.firstName}
                    lastName={passanger.lastName}
                    dateOfBirth={dayjs(passanger['dateOfBirth'])}
                    onEdit={handleEditPassenger}
                    key={passanger.id}
                    edited={passanger.edited}
                />)
            }
        </>
    );
};

export default Booking;
