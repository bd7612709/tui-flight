import { checkUpdateName } from './checkUpdateName';

describe('checkUpdateName', () => {
  it('should return true if the names are within 3 edits', () => {
    expect(checkUpdateName('abcdefsc', 'ablkefsc')).toBe(true);
    expect(checkUpdateName('John', 'JohnDoe')).toBe(true);
  });

  it('should return false if the names require more than 3 edits', () => {
    expect(checkUpdateName('abcdef', 'xyxabc')).toBe(false);
    expect(checkUpdateName('Jonathan', 'Jon')).toBe(false);
  });
});