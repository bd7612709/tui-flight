import { Dayjs } from 'dayjs';

export interface Passenger {
    id?: string,
    title: string,
    gender: string,
    firstName: string,
    lastName: string,
    dateOfBirth: string,
    edited?: boolean,
};

export interface PassengerCardProps extends Omit<Passenger, 'dateOfBirth'> {
    dateOfBirth?: Dayjs | null;
    onEdit?: Function,
};
