import React from 'react';
import { AppBar, Typography, Grid, Box } from '@mui/material';

import Item from './Item';

import Logo from '../../assets/images/logo.svg';

const Header = () => {
    return (
        <AppBar position="static" sx={{ width: '100vw' }}>
            <Typography
                sx={{
                    backgroundColor: '#6fcbf5',
                }}
                padding={'0.8rem 1.5rem'}
            >
                <img width='50' src={Logo} alt='logo' />
            </Typography>

            <Typography
                sx={{
                    backgroundColor: '#11295e',
                }}
                padding={'0.8rem 0.5rem'}
                component="div"
            >
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} md={5}>
                            <Grid container spacing={1}>
                                <Grid item xs={12} sm={6} md={3}>
                                    <Item title='ZY1ZYT' description='BOOKING ID' />
                                </Grid>
                                <Grid item xs={12} sm={6} md={3}>
                                    <Item title='04.092023 16:26' description='DATE OF BOOKING' />
                                </Grid>
                                <Grid item xs={12} sm={6} md={3}>
                                    <Item title='Confirmed' description='STATUS' />
                                </Grid>
                                <Grid item xs={12} sm={6} md={3}>
                                    <Item title='428,98' description='TOTAL IN EUR' />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} md={7}>
                            Hello
                        </Grid>
                    </Grid>
                </Box>
            </Typography>
        </AppBar>
    );
};

export default Header;
