import { render, screen } from '@testing-library/react';
import Header from './index';

describe('Header Component', () => {
    test('renders Header component', () => {
        render(<Header />);

        const logoElement = screen.getByAltText(/logo/i);
        expect(logoElement).toBeInTheDocument();

        const bookingIdElement = screen.getByText('BOOKING ID');
        const dateOfBookingElement = screen.getByText('DATE OF BOOKING');
        const statusElement = screen.getByText('STATUS');
        const totalInEurElement = screen.getByText('TOTAL IN EUR');

        expect(bookingIdElement).toBeInTheDocument();
        expect(dateOfBookingElement).toBeInTheDocument();
        expect(statusElement).toBeInTheDocument();
        expect(totalInEurElement).toBeInTheDocument();

    });
});
