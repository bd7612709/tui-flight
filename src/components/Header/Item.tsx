import { Box, Grid, Typography } from '@mui/material';
import React from 'react';

interface PropsTypes {
    title?: string,
    description?: string,
};

const HeaderItem = ({ title, description }: PropsTypes) => {
    return (
        <Box>
            <Grid container textAlign={'center'}>
                <Grid item sm={12} md={12}>
                    <Typography variant={'body1'} component={'div'}>
                        {title}
                    </Typography>
                </Grid>
                <Grid item sm={12} md={12}>
                    <Typography variant={'caption'} component={'div'} color={'#8c98b1'}>
                        {description}
                    </Typography>
                </Grid>
            </Grid>
        </Box>
    );
};

export default HeaderItem;
