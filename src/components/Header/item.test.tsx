import { render, screen } from '@testing-library/react';
import HeaderItem from './Item';

describe('HeaderItem Component', () => {
    test('renders HeaderItem component with title and description', () => {
        const title = 'Test Title';
        const description = 'Test Description';

        render(<HeaderItem title={title} description={description} />);

        const titleElement = screen.getByText(title);
        const descriptionElement = screen.getByText(description);

        expect(titleElement).toBeInTheDocument();
        expect(descriptionElement).toBeInTheDocument();
    });
});
