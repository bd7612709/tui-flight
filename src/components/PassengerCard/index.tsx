import React, { ChangeEvent, useEffect, useState } from 'react';
import { 
    Accordion, 
    AccordionSummary, 
    AccordionDetails, 
    AccordionActions, 
    Button, 
    Grid, 
    Card, 
    CardContent, 
    Typography,
    TextField,
    MenuItem,
} from '@mui/material';
import { useSnackbar } from 'notistack';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Dayjs } from 'dayjs';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { PassengerCardProps } from '../../types/types';
import ConfirmModal from '../ConfirmModal';
import { checkUpdateName } from '../../utils/checkUpdateName/checkUpdateName';

const PassengerCard = ({
    id,
    title,
    gender,
    firstName,
    lastName,
    dateOfBirth,
    onEdit,
    edited,
} : PassengerCardProps) => {
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [data, setData] = useState<PassengerCardProps>({
        title: '',
        gender: 'MALE',
        firstName: '',
        lastName: '',
        dateOfBirth: null,
    });
    const [isEdited, setIsEdited] = useState<boolean>(false);
    const { enqueueSnackbar } = useSnackbar();

    const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        });
        setIsEdited(true);
        if(
            (e.target.name === 'firstName' && !checkUpdateName(firstName, e.target.value)) ||
            (e.target.name === 'lastName' && !checkUpdateName(lastName, e.target.value))
        ) {
            enqueueSnackbar(`Can't be changed more than 3 letters of name!`, { variant: 'warning' });
            setIsEdited(false);
            return;
        }
    };

    const handleSelect = (e: any) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        });
        setIsEdited(true);
    };

    const submitEdit = () => {
        setModalOpen(true);
    };
    
    const handleConfirm = () => {
        if(edited) {
            setData((prevData) => ({
                ...prevData,
                title: title || '',
                gender: gender || 'Male',
                firstName: firstName || '',
                lastName: lastName || '',
                dateOfBirth: dateOfBirth || null,
            }));
            enqueueSnackbar(`The passenger was updated once already. You can't edit this passenger any more!`, { variant: 'warning' });
            return;
        }
        if (!onEdit) return;

        const editedNewly = onEdit({ id, ...data, dateOfBirth: data.dateOfBirth?.format('YYYY-MM-DD') });
        setModalOpen(false);
        if (editedNewly) setIsEdited(false);
    };

    const handleCancel = () => {
        setData((prevData) => ({
            ...prevData,
            title: title || '',
            gender: gender || 'Male',
            firstName: firstName || '',
            lastName: lastName || '',
            dateOfBirth: dateOfBirth || null,
        }));
        setModalOpen(false);
    };

    const handleDatePicker = (value: Dayjs | null) => {
        setData({
            ...data,
            dateOfBirth: value,
        });
        setIsEdited(true);
    };

    useEffect(() => {
        setData((prevData) => ({
            ...prevData,
            title: title || '',
            gender: gender || 'Male',
            firstName: firstName || '',
            lastName: lastName || '',
            dateOfBirth: dateOfBirth || null,
        }))
    }, [title, gender, firstName, lastName, dateOfBirth]);

    return (
        <>
            <Accordion sx={{ margin: '10px 0', border: '2px solid #212121' }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1-content"
                    id="panel1-header"
                    style={{ padding: '0 35px' }}
                >
                    <Typography color={'#11295e'} fontWeight={600}>
                        PASSENGER DETAIL
                    </Typography>
                </AccordionSummary>
                <AccordionDetails style={{ padding: '20px 35px', backgroundColor: '#e2f3fe' }}>
                    <Card>
                        <CardContent>
                            <Typography sx={{ fontSize: 14 }} color='#11295e' gutterBottom>
                                Edit passenger
                            </Typography>

                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={3} md={2.5} margin={'10px 0'}>
                                    <TextField
                                        select
                                        fullWidth 
                                        variant='standard'
                                        label='Salutation'
                                        name="title"
                                        value={data.title}
                                        onChange={handleSelect}
                                    >
                                        <MenuItem value={'MRS'}>MRS</MenuItem>
                                        <MenuItem value={'MR'}>MR</MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item xs={12} sm={4} md={3.5} margin={'10px 0'}>
                                    <TextField 
                                        fullWidth 
                                        id="standard-basic" 
                                        label="Firstname" 
                                        variant="standard"
                                        value={data.firstName}
                                        name="firstName"
                                        onChange={handleChange}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={4} md={3.5} margin={'10px 0'}>
                                    <TextField 
                                        fullWidth 
                                        id="standard-basic" 
                                        label="Lastname"
                                        variant="standard"
                                        value={data.lastName}
                                        name="lastName"
                                        onChange={handleChange}
                                    />
                                </Grid>
                            </Grid>

                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={3} md={2.5} margin={'10px 0'}>
                                    <TextField
                                        variant='standard'
                                        label='Gender'
                                        value={data.gender}
                                        name="gender"
                                        onChange={handleSelect}
                                    >
                                        <MenuItem value={'MALE'}>Male</MenuItem>
                                        <MenuItem value={'FEMALE'}>Female</MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item xs={12} sm={4} md={3.5} margin={'10px 0'}>
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DatePicker
                                            label="Date of birth" 
                                            slotProps={{ textField: { variant: 'standard', fullWidth: true } }}
                                            format='YYYY-MM-DD'
                                            value={data.dateOfBirth}
                                            onChange={handleDatePicker}
                                        />
                                    </LocalizationProvider>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </AccordionDetails>

                <AccordionActions sx={{ padding: '0 35px 20px', backgroundColor: '#e2f3fe' }}>
                    <Grid container justifyContent={'space-between'}>
                        <Grid item md={3} display={'flex'} justifyContent={'flex-start'}>
                            <Button
                                sx={{
                                    backgroundColor: '#11295e'
                                }}
                                fullWidth
                                size={'large'}
                                variant={'contained'}
                            >
                                BACK
                            </Button>
                        </Grid>
                        <Grid item md={3} display={'flex'} justifyContent={'flex-end'}>
                            <Button
                                sx={{
                                    backgroundColor: '#11295e'
                                }}
                                fullWidth 
                                size={'large'} 
                                variant={'contained'}
                                onClick={submitEdit}
                                disabled={!isEdited}
                            >
                                EDIT
                            </Button>
                        </Grid>
                    </Grid>
                </AccordionActions>
            </Accordion>
            <ConfirmModal open={modalOpen} onConfirm={handleConfirm} onCancel={handleCancel} />
        </>
    );
};

export default PassengerCard;
