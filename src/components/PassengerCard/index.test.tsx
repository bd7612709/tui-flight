import { render, screen, fireEvent } from '@testing-library/react';
import PassengerCard from './index';

describe('PassengerCard Component', () => {
  test('renders PassengerCard component and interacts with elements', () => {
    render(<PassengerCard id="1" title="MR" gender="MALE" firstName="John" lastName="Doe" dateOfBirth={null} onEdit={jest.fn()} />);

    // Check if the elements are rendered
    expect(screen.getByText('PASSENGER DETAIL')).toBeInTheDocument();
    expect(screen.getByText('Edit passenger')).toBeInTheDocument();
    expect(screen.getByLabelText('Salutation')).toBeInTheDocument();
    expect(screen.getByLabelText('Firstname')).toBeInTheDocument();
    expect(screen.getByLabelText('Lastname')).toBeInTheDocument();
    expect(screen.getByLabelText('Gender')).toBeInTheDocument();
    expect(screen.getByLabelText('Date of birth')).toBeInTheDocument();
    expect(screen.getByText('BACK')).toBeInTheDocument();
    expect(screen.getByText('EDIT')).toBeInTheDocument();

    // Interact with the elements
    const editButton = screen.getByText('EDIT');
    fireEvent.click(editButton);

    // Add more interaction tests as needed
  });
});