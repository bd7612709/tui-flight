import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import ConfirmModal from './index';

describe('ConfirmModal Component', () => {
  test('renders ConfirmModal component and triggers events', () => {
    const onConfirmMock = jest.fn();
    const onCancelMock = jest.fn();

    render(
      <ConfirmModal
        open={true}
        onConfirm={onConfirmMock}
        onCancel={onCancelMock}
      />
    );

    const cancelButton = screen.getByText('Cancel');
    const okButton = screen.getByText('Ok');

    fireEvent.click(cancelButton);
    fireEvent.click(okButton);

    expect(onCancelMock).toHaveBeenCalledTimes(1);
    expect(onConfirmMock).toHaveBeenCalledTimes(1);
  });
});