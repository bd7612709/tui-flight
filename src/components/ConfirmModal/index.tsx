import React from 'react';
import { Button, Dialog, DialogActions, DialogTitle } from '@mui/material';

interface PropsTypes {
    open?: boolean,
    onConfirm?: React.MouseEventHandler<HTMLButtonElement> | undefined,
    onCancel?: React.MouseEventHandler<HTMLButtonElement> | undefined,
};

const ConfirmModal = ({ open, onConfirm, onCancel }: PropsTypes) => {
    return (
        <Dialog open={open || false}>
            <DialogTitle>
                Are you really going to edit a passneger?
            </DialogTitle>
            <DialogActions>
                <Button variant={'contained'} sx={{ backgroundColor: '#ba3434' }} onClick={onCancel}>Cancel</Button>
                <Button variant={'contained'} sx={{ backgroundColor: '#11295e' }} onClick={onConfirm}>Ok</Button>
            </DialogActions>
        </Dialog>
    );
};

export default ConfirmModal;
