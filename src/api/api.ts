import { Passenger } from '../types/types';

let passengers: Passenger[] = [
    {
        "id": "1",
        "title": "MRS",
        "gender": "FEMALE",
        "firstName": "Jane",
        "lastName": "Doe",
        "dateOfBirth": "2003-08-31",
        "edited": false,
    },
    {
        "id": "2",
        "title": "MR",
        "gender": "MALE",
        "firstName": "John",
        "lastName": "Doe",
        "dateOfBirth": "2001-04-12",
        "edited": false,
    }
];

export const allPassengersApi = (): Promise<Passenger[]> => {
    return new Promise((resolve, reject) => {
        resolve(passengers);
    });
};

export const editPassengerApi = ({ id, title, gender, firstName, lastName, dateOfBirth }: Passenger): Promise<Passenger> => {
    return new Promise((resolve, reject) => {
        try {
            const _passengers = passengers.map((passenger) => {
                if (passenger.id === id) {
                    return {
                        id,
                        title,
                        gender,
                        firstName,
                        lastName,
                        dateOfBirth,
                        edited: true,
                    };
                }
                else return passenger;
            });
    
            passengers = [ ..._passengers ];

            resolve({ id, title, gender, firstName, lastName, dateOfBirth });
        } catch (error: any) {
            reject(error.message);
        }
    });
};
