import { Container } from '@mui/material';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';

import { store } from './store';
import Header from './components/Header';
import Booking from './pages/Booking';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />

        <Container sx={{ marginTop: '1rem', width: '100vw' }}>
          <SnackbarProvider maxSnack={3}>
            <Booking />
          </SnackbarProvider>
        </Container>
      </div>
    </Provider>
  );
};

export default App;
