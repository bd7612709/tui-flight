import { createSlice, createAsyncThunk, SliceCaseReducers } from '@reduxjs/toolkit';

import { Passenger } from '../types/types';
import { allPassengersApi, editPassengerApi } from "../api/api";

export interface StateType {
    fetching: boolean;
    fetched: boolean;
    fetchedError: null | any;
    passengers: Passenger[],
    editing: boolean;
    edited: boolean;
    editedError: null | any;
}

export const fetechPassengers = createAsyncThunk(
    'passgenger/all',
    async () => {
        try {
            const response = await allPassengersApi();
            return response;
        } catch (err) {
            console.error(err);
            throw err;
        };
    }
);

export const editPassenger = createAsyncThunk(
    'passenger/edit',
    async ({ id, title, gender, firstName, lastName, dateOfBirth }: Passenger) => {
        try {
            const response = await editPassengerApi({ id, title, gender, firstName, lastName, dateOfBirth });

            return response;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
);

const passengerSlice = createSlice<StateType, SliceCaseReducers<StateType>>({
    name: "passenger",
    initialState: {
        fetching: false,
        fetched: false,
        fetchedError: null,
        passengers: [],
        editing: false,
        edited: false,
        editedError: null,
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetechPassengers.pending, (state) => {
                state.fetching = true;
            })
            .addCase(fetechPassengers.fulfilled, (state, action) => {
                state.fetching = false;
                state.fetched = true;
                state.passengers = action.payload;
            })
            .addCase(fetechPassengers.rejected, (state, action) => {
                state.fetching = false;
                state.fetched = false;
                state.fetchedError = action.payload;
            })
            .addCase(editPassenger.pending, (state) => {
                state.editing = true;
            })
            .addCase(editPassenger.fulfilled, (state, action) => {
                state.editing = false;
                state.edited = true;
                state.editedError = null;
            })
            .addCase(editPassenger.rejected, (state, action) => {
                state.editing = false;
                state.edited = false;
                state.editedError = action.payload;
            });
    },
})

export default passengerSlice.reducer;
