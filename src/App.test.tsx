import ReactDOM from 'react-dom/client';
import App from './App';

it('renders without crashing', () => {
  const root = document.createElement('div');
  const rootContainer = ReactDOM.createRoot(root);
  rootContainer.render(<App />);
});